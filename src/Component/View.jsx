import React, { Component } from "react";
import axios from "axios";
import { Container, Row, Col, Image} from "react-bootstrap";

export default class View extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      description: "",
      image: "",
    };
  }
  componentWillMount() {
    console.log(this.props.match.params.id);
    axios
      .get(
        `http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`
      )
      .then((res) =>
        this.setState({
          title: res.data.DATA.TITLE,
          description: res.data.DATA.DESCRIPTION,
          image: res.data.DATA.IMAGE,
        })
      );
  }
  render() {
    return (
      <div style={{ width: "80%", margin: "auto" }}>
        <Container>
          <Row>
            <Col lg={8}>
              <h4>Title</h4>
              <Row>{this.state.title}</Row>
              <h4>Description</h4>
              <Row>{this.state.description}</Row>
            </Col>
            <Col lg={4}>
                {/* <img src={this.state.image} alt ="its a pic bruh. what do you expect???" ></img> */}
                <Image className="img" src={this.state.image} alt ="its a pic bruh. what do you expect???" thumbnail />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
