import React, { Component } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import axios from "axios";

export default class Edit extends Component {
  constructor() {
    super();
    this.state = {
      title: "",
      description: "",
      image: "",
      titleError: "",
      descriptionError: "",
    };
  }

  componentWillMount() {
    console.log(this.props.match.params.id);
    axios
      .get(
        `http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`
      )
      .then((res) =>
        this.setState({
          title: res.data.DATA.TITLE,
          description: res.data.DATA.DESCRIPTION,
          image: res.data.DATA.IMAGE,
        })
      );
  }

  changeDescription = (event) => {
    this.setState({
      description: event.target.value,
    });
  };

  changeTitle = (event) => {
    this.setState({
      title: event.target.value,
    });
  };

  handleClick() {
    let article = {
      TITLE: this.state.title,
      DESCRIPTION: this.state.description,
    };
    if (article.TITLE === "") {
      this.setState({
        titleError: "field can't be blank",
      });
    }
    if (article.DESCRIPTION === "") {
      this.setState({
        descriptionError: "field can't be blank",
      });
    }
    if (article.TITLE !== "" && article.DESCRIPTION !== "") {
      axios
        .put(
          `http://110.74.194.124:15011/v1/api/articles/${this.props.match.params.id}`,
          article
        )
        .then((res) => {
          alert(res.data.MESSAGE);
        });
      this.setState({
        title: "",
        description: "",
        titleError: "",
        descriptionError: ""
      });
    }
  }

  render() {
    return (
      <div style={{ width: "80%", margin: "auto" }}>
        <h2>Update Article</h2>
        <Container>
          <Row>
            <Col lg={8}>
              <Form>
                <Form.Group>
                  <Form.Label>Title</Form.Label>
                  <Form.Control
                    name="title"
                    type="text"
                    // placeholder="Enter Title"
                    value={this.state.title}
                    onChange={this.changeTitle}
                  />
                  <h6 style={{color : "red"}}>{this.state.titleError}</h6>
                </Form.Group>

                <Form.Group>
                  <Form.Label>Description</Form.Label>
                  <Form.Control
                    name="description"
                    type="text"
                    // placeholder="Description"
                    value={this.state.description}
                    onChange={this.changeDescription}
                  />
                  <h6 style={{color : "red"}}>{this.state.descriptionError}</h6>
                </Form.Group>
                <Button
                  variant="primary"
                  type="button"
                  onClick={() => this.handleClick()}
                >
                  Edit
                </Button>
              </Form>
            </Col>
            <Col lg={4}></Col>
          </Row>
        </Container>
      </div>
    );
  }
}
