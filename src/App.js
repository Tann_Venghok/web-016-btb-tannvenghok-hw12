import React, { Component } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./Component/Home";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AddArticle from "./Component/AddArticle";
import List from "./Component/List";
import axios from "axios"
import Edit from "./Component/Edit";
import View from "./Component/View";

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      articles: [],
    };
  }
  componentWillMount() {
    axios
      .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          articles: res.data.DATA,
        });
      });
  }

  componentWillUpdate(){
    axios
      .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          articles: res.data.DATA,
        });
      });
  }
  render() {
    return (
      <div className="App">
      <Router>
      {/* Home is actually navbar */}
      <Home/>
        <Switch>
          <Route path="/" exact render={()=><List data={this.state.articles}/>} />
          <Route path ="/add" exact component={AddArticle}/>
          <Route path ="/edit/id=:id" exact component={Edit}/>
          <Route path = "/view/id=:id" component={View}/>
        </Switch>
        </Router>
      </div>
    );
  }
}
